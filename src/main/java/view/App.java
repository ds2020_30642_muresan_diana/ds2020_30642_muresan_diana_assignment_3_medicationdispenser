package view;

import model.Medication;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class App {

    private JFrame frame = new JFrame("App");
    private JScrollPane scrollPane = new JScrollPane();
    private JPanel pan = new JPanel();
    private ArrayList<String[]> data = null;
    private JPanel rootPanel;
    private ArrayList<String[]> displayData = new ArrayList<String[]>();
    private Timer timer;
    private Date date;
    private Medication medication;



    public App() {

        rootPanel = new JPanel();
        frame.setBounds(100, 100, 1200, 800);
        frame.getContentPane().add(rootPanel);
        frame.setLocationRelativeTo(null);
        frame.getContentPane().setBackground(new Color(253, 249, 249) );

        rootPanel.setLayout(new GridLayout(0, 1));



        JLabel title = new JLabel("Medication Plans", JLabel.CENTER);
        title.setFont(new Font("Cambria", Font.BOLD, 30));
        rootPanel.add(title);


        GridLayout gridLayout = new GridLayout(0, 3);
        gridLayout.setHgap(10);
        gridLayout.setVgap(10);
        pan = new JPanel();
        pan.setSize(600, 300);
        pan.setLayout(gridLayout);

        JPanel columns = new JPanel();
        columns.setLayout(gridLayout);
        JLabel col1 = new JLabel("Medication");
        JLabel col2 = new JLabel("Intake Intervals");
        JLabel col3 = new JLabel("Taken");
        col1.setFont(new Font("Cambria", Font.BOLD, 18));
        col2.setFont(new Font("Cambria", Font.BOLD, 18));
        col3.setFont(new Font("Cambria", Font.BOLD, 18));
        columns.add(col1);
        columns.add(col2);
        columns.add(col3);

        rootPanel.add(columns);
        scrollPane.setViewportView(pan);
        rootPanel.add(scrollPane);

        JLabel header = new JLabel("", JLabel.LEFT);
        header.setFont(new Font("Cambria", Font.BOLD, 50));
        rootPanel.add(header);
        ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                date = new Date();
                DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
                String time = timeFormat.format(date);
                header.setText(time);

                if (data!=null) {
                    checkIntervals();
                }

                if (date.getHours() == 23 && date.getMinutes() == 55 && date.getSeconds() == 0) {
                    Date newDate = new Date();
                    newDate.setDate(21);
                    UUID patientId = UUID.fromString("2dc466a6-2ace-4563-9bf6-da9bd4268a26");
                    ArrayList<String[]> newPlans = medication.sendMedication(newDate, patientId);
                    setData(newPlans);
                    destroy();

                }



            }
        };
        timer = new Timer(1000, actionListener);
        timer.setInitialDelay(0);
        timer.start();



        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(false);

    }

    public void destroy() {
        scrollPane.remove(pan);

       // scrollPane.remove(pan);
        pan = new JPanel();
        scrollPane.add(pan);


        frame.revalidate();

        viewMeds();

    }
    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public void checkIntervals() {
        ArrayList<String[]> toRemove = new ArrayList<String[]>();
        for (String[] med:data) {
            String hours[] = med[1].split("-");
            String lowerLimit[] = hours[0].split(":");
            String upperLimit[] = hours[1].split(":");
            Date interval1 = new Date();
            Date interval2 = new Date();
            interval1.setHours(Integer.parseInt(lowerLimit[0]));
            interval1.setMinutes(Integer.parseInt(lowerLimit[1]));
            interval1.setSeconds(0);
            interval2.setHours(Integer.parseInt(upperLimit[0]));
            interval2.setMinutes(Integer.parseInt(upperLimit[1]));
            interval2.setSeconds(0);

            if (date.after(interval2)) {
                 JOptionPane.showMessageDialog(frame, "Medication " + med[0]+ " not taken", "Alert",JOptionPane.ERROR_MESSAGE);

                 toRemove.add(med);
                 String message = "Medication " + med[0] + " not taken in the specified interval";
                 medication.getMessage(message);
            }
        }
       data.removeAll(toRemove);
    }

    public void viewMeds() {

            for (int i = 0; i < this.displayData.size(); i++) {
                String[] med = displayData.get(i);
                JTextArea area = new JTextArea(2, 10);
                area.setText(med[0]);
                area.setEditable(false);
                area.setFont(new Font("Cambria", Font.BOLD, 18));
                pan.add(area);
                JTextArea area1 = new JTextArea(2, 10);
                area1.setEditable(false);
                area1.setText(med[1]);
                area1.setFont(new Font("Cambria", Font.BOLD, 18));
                pan.add(area1);
                JButton button = new JButton("Take");
                button.setBackground(new Color(51, 153, 255));
                button.setFont(new Font("Cambria", Font.BOLD, 18));
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String hours[] = med[1].split("-");
                        String lowerLimit[] = hours[0].split(":");
                        String upperLimit[] = hours[1].split(":");
                        Date interval1 = new Date();
                        Date interval2 = new Date();
                        interval1.setHours(Integer.parseInt(lowerLimit[0]));
                        interval1.setMinutes(Integer.parseInt(lowerLimit[1]));
                        interval1.setSeconds(0);

                        interval2.setHours(Integer.parseInt(upperLimit[0]));
                        interval2.setMinutes(Integer.parseInt(upperLimit[1]));
                        interval2.setSeconds(0);

                        if (interval1.before(date) && interval2.after(date)) {
                            if (displayData.size() == 1 ) {
                                area.setVisible(false);
                                area1.setVisible(false);
                                button.setVisible(false);
                            }
                            pan.remove(area);
                            pan.remove(area1);
                            pan.remove(button);
                            data.remove(med);
                            displayData.remove(med);
                            System.out.println(displayData.size());
                            String message = "Medication " + med[0] + " taken correctly";
                            medication.getMessage(message);


                        }
                        else {
                            JOptionPane.showMessageDialog(frame, "Medication was not taken at the correct time", "Alert",JOptionPane.ERROR_MESSAGE);
                            String message = "Medication " + med[0] + " not taken correctly";
                            medication.getMessage(message);

                        }

                    }
                });
                pan.add(button);
            }




    }
    public void setData(ArrayList<String[]> data) {
        ArrayList<String[]> newD = new ArrayList<String[]>();
       /* for (String[] med: data) {
            String hours[] = med[1].split("-");
            String lowerLimit[] = hours[0].split(":");
            String upperLimit[] = hours[1].split(":");
            Date interval1 = new Date();
            Date interval2 = new Date();
            interval1.setHours(Integer.parseInt(lowerLimit[0]));
            interval1.setMinutes(Integer.parseInt(lowerLimit[1]));
            interval1.setTime(0);

            interval2.setHours(Integer.parseInt(upperLimit[0]));
            interval2.setMinutes(Integer.parseInt(upperLimit[1]));
            interval2.setSeconds(0);

            if (interval1.before(date) && interval2.after(date))
            {
                newD.add(med);
            }
        }*/
        this.data = data;

        this.displayData = data;
        viewMeds();
        frame.setVisible(true);
    }




}
