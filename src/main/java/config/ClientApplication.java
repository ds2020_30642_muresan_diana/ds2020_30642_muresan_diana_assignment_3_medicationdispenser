package config;

import controller.AppController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;

import model.Medication;
import view.App;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@SpringBootApplication
public class ClientApplication {

    @Bean
    public HessianProxyFactoryBean hessianInvoker() {
        HessianProxyFactoryBean invoker = new HessianProxyFactoryBean();
        invoker.setServiceUrl("http://localhost:8080/hellohessian");
        invoker.setServiceInterface(Medication.class);
        return invoker;
    }

    public static void main(String[] args) {

        App appView = new App();
        AppController appController = new AppController(appView);
        ConfigurableApplicationContext context =  SpringApplication.run(ClientApplication.class, args);
        System.out.println("========Client Side===============");
        Medication medication = context.getBean(Medication.class);

        UUID patientId = UUID.fromString("2dc466a6-2ace-4563-9bf6-da9bd4268a26");
        ArrayList<String[]> medPlans = medication.sendMedication(new Date(), patientId);
        System.out.println(medPlans.size());
        for (String[] medPlan:medPlans) {
            System.out.println(medPlan[0] + " " + medPlan[1]);
        }

        appController.transmitData(medPlans);
        appController.setMedication(medication);

    }
}
