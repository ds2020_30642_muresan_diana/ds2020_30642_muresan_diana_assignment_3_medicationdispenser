package controller;

import model.Medication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import view.App;

import java.util.ArrayList;
import java.util.List;


public class AppController {
    private App appView;
    private Medication medication;

    public AppController(App appView) {
        this.appView = appView;

    }

    public void transmitData(ArrayList<String[]> data) {

        this.appView.setData(data);
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
        this.appView.setMedication(medication);
    }




}
